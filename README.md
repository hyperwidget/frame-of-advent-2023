## Hi!

This project is _super_ under construction and as such may break and _drastically_ change at any time. As a result, documentation is going to be sparse until things settle somewhat(day 10ish seems likely), because I don't want to rewrite everything

# Intro Blurb

Hi! I started this year's advent of code late and without any thoughts on how I was going to tackle it, so I started with an online editor using JS. After completing the first two days, I wanted a better way to iterate answers and test cases, share solutions and maybe some sort of visualizations.

So I guess I'm making a framework? Also without any plans or ideas?

I think a goal is to try to work within this framework to solve the advent of code answers while also building out an increasingly more and more unnecessarily complex, but terrible series of tools and systems, while also building out a rough site to put it on the internet. This will not be pretty; fun, but not pretty.

Edit Dec 5: I worked the solution mostly in the framework today, hit major annoyances, and fixed them. Spent a lot of time working on the frameworky parts of things and fleshing out the basic functionality. I think the majority of the basic ideas are in place; and I'll try to work the problem mostly in the framework again tomorrow. Hopefully there will be fewer major issues and I'll consider some sort of visualization?

# Projecty Things

## Technology Used

- React
- EmotionJS
- React-ace
- Antd
- Netlify

## Project Tooling

### Generate Day

This is a node script that scaffolds out all the necessary files to begin working on the next day. Invoked via the below command in the terminal

`node -e 'require("./tools/scripts/scaffoldDay").generateNewDay({DAY_NUMBER})`

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
