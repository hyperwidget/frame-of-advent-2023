export const day2 = {
  part1: `(input) => {
    const scoreMap = {
      A: 1,
      B: 2,
      C: 3,
      X: 1,
      Y: 2,
      Z: 3,
    };

    let myScore = 0;

    const rounds = input.split(/\\r?\\n/).map((val) => val.trim());

    rounds.forEach((round) => {
      const [them, me] = round.split(" ");
      let theirVal = scoreMap[them];
      let myVal = scoreMap[me];

      if (theirVal > myVal) {
        if (theirVal === 3 && myVal === 1) {
          myVal += 6;
        } else {
          theirVal += 6;
        }
      } else if (myVal > theirVal) {
        if (theirVal === 1 && myVal === 3) {
          theirVal += 6;
        } else {
          myVal += 6;
        }
      } else if (myVal === theirVal) {
        theirVal += 3;
        myVal += 3;
      }
      myScore += myVal;
    });

    return myScore;
  }`,
  part2: `(input) => {
    const scoreMap = {
      A: 1,
      B: 2,
      C: 3,
      X: 1,
      Y: 2,
      Z: 3,
    };

    let myScore = 0;

    const rounds = input.split(/\\r?\\n/);

    rounds.forEach((round) => {
      const [them, outcome] = round.trim().split(" ");
      const theirVal = scoreMap[them];
      let myVal = 0;

      // lose
      if (outcome === "X") {
        switch (theirVal) {
          case 1: {
            myVal += 3;
            break;
          }
          case 2: {
            myVal += 1;
            break;
          }
          case 3: {
            myVal += 2;
            break;
          }
          default:
        }
        // draw
      } else if (outcome === "Y") {
        myVal += theirVal + 3;
        // win
      } else {
        switch (theirVal) {
          case 1: {
            myVal += 2 + 6;
            break;
          }
          case 2: {
            myVal += 3 + 6;
            break;
          }
          case 3: {
            myVal += 1 + 6;
            break;
          }
          default:
        }
      }

      myScore += myVal;
    });

    return myScore;
  }`,
};
