export const day6 = {
  part1: `(input) => {
    let index = 0;

    for (let i = 4; i < input.length; i++) {
      const segment = input.slice(i - 4, i);
      const segmentSet = new Set(segment.split(""));
      if (segmentSet.size === 4) {
        const remainingString = input.slice(i);
        if (remainingString.indexOf(index[i] > -1)) {
          index = i;
          i = input.length;
        }
      }
    }

    return index;
  }`,
  part2: `(input) => {
    let index = 0;

    for (let i = 14; i < input.length; i++) {
      const segment = input.slice(i - 14, i);
      const segmentSet = new Set(segment.split(""));
      if (segmentSet.size === 14) {
        const remainingString = input.slice(i);
        if (remainingString.indexOf(index[i] > -1)) {
          index = i;
          i = input.length;
        }
      }
    }

    return index;
  }`,
};
