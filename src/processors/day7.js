export const day7 = {
  part1: `input => {
    const lines = input.split(/\\r?\\n/).map(val => val.trim());
    
    const recursor = (directory, folder) => {
      let tally = 0;
      folder.children.forEach(child => {
        const childData = directory[child];
        tally += childData.fileTotal
        const childTotal = recursor(directory, childData);
        childData.childTotal = childTotal;
        tally += childTotal;
      });
      
      return tally;
    };

    const directory = {};
    let currentDirectory = "";
    const eligibleDirectories = [];

    lines.forEach((line, i) => {
      // terminal instruction
      if (line[0] === "$") {
        const [sign, instruction, dir] = line.split(" ");
        if (instruction === "cd") {
          if (dir === "..") {
            currentDirectory = directory[currentDirectory].parent;
          } else if (!directory[dir]) {
            directory[\`\${currentDirectory}.\${dir}\`] = {
              parent: currentDirectory,
              children: [],
              files: [],
              fileTotal: 0
            };
            currentDirectory = \`\${currentDirectory}.\${dir}\`;
          }
        }
      } else {
        const [detail, name] = line.split(" ");
        if (detail === "dir") {
          directory[currentDirectory].children.push(\`\${currentDirectory}.\${name}\`);
        } else {
          directory[currentDirectory].files.push([detail, name]);
          directory[currentDirectory].fileTotal += parseInt(detail);
        }
      }
    });
    Object.keys(directory).forEach(name => {
      const folder = directory[name];

      let encompasingTotal = folder.fileTotal;
      encompasingTotal += recursor(directory, folder);
      folder.encompasingTotal = encompasingTotal;

      if (encompasingTotal < 100000) {
        eligibleDirectories.push(folder);
      }
    });

    return eligibleDirectories.reduce((acc, val) => {
      return acc += val.encompasingTotal;
    }, 0);
  }
  `,
  part2: `input => {
  const lines = input.split(/\\r?\\n/).map(val => val.trim());
  const maxSpace = 70000000;
  const targetSpace = 30000000;
  
  const recursor = (directory, folder) => {
    let tally = 0;
    folder.children.forEach(child => {
      const childData = directory[child];
      tally += childData.fileTotal
      const childTotal = recursor(directory, childData);
      childData.childTotal = childTotal;
      tally += childTotal;
    });
    
    return tally;
  };
  
  const directory = {};
  let currentDirectory = "";


  lines.forEach((line, i) => {
    // terminal instruction
    if (line[0] === "$") {
      const [sign, instruction, dir] = line.split(" ");
      if (instruction === "cd") {
        if (dir === "..") {
          currentDirectory = directory[currentDirectory].parent;
        } else if (!directory[dir]) {
          directory[\`\${currentDirectory}.\${dir}\`] = {
            parent: currentDirectory,
            children: [],
            files: [],
            fileTotal: 0
          };
          currentDirectory = \`\${currentDirectory}.\${dir}\`;
        }
      }
    } else {
      const [detail, name] = line.split(" ");
      if (detail === "dir") {
        directory[currentDirectory].children.push(\`\${currentDirectory}.\${name}\`);
      } else {
        directory[currentDirectory].files.push([detail, name]);
        directory[currentDirectory].fileTotal += parseInt(detail);
      }
    }
  });
  Object.keys(directory).forEach(name => {
    const folder = directory[name];

    let encompasingTotal = folder.fileTotal;
    encompasingTotal += recursor(directory, folder);
    folder.encompasingTotal = encompasingTotal;
  });
  
  const totalUsedSpace = directory["./"].encompasingTotal
  const availableSpace = maxSpace - totalUsedSpace
  const neededSpace = targetSpace - availableSpace
  
  

  const eligibleFolders = Object.values(directory).filter(val => val.encompasingTotal > neededSpace)
  const eligibleValues = eligibleFolders.map(folder => folder.encompasingTotal)

  
  return eligibleValues.sort((a,b) => a-b)[0]
}`,
};
