export const day3 = {
  part1: `(input) => {
    const sacks = input
      .trim()
      .split(/\\r?\\n/)
      .map((val) => val.trim());

    const dupes = [];

    sacks.forEach((sack) => {
      const compartmentLength = sack.length / 2;

      const comp1 = sack.slice(0, compartmentLength);
      const comp2 = sack.slice(compartmentLength);

      let dupe = "";

      for (let i = 0; i < comp2.length; i++) {
        if (comp2.indexOf(comp1[i]) > -1) {
          dupe = comp1[i];
          i = comp2.length;
        }
      }

      dupes.push(dupe);
    });

    let total = 0;

    dupes.forEach((dupe) => {
      if (dupe === dupe.toLowerCase()) {
        // The character is lowercase
        total += parseInt(dupe, 36) - 9;
      } else {
        // The character is uppercase
        total += parseInt(dupe, 36) + 17;
      }
    });
    return total;
  }`,
  part2: `(input) => {
    const sacks = input
      .trim()
      .split(/\\r?\\n/)
      .map((val) => val.trim());

    const groups = [];

    let acc = [];
    for (let i = 1; i <= sacks.length; i++) {
      acc.push(sacks[i - 1]);
      if (i % 3 === 0) {
        groups.push(acc);
        acc = [];
      }
    }

    const dupes = [];

    groups.forEach((group) => {
      const [sack1, sack2, sack3] = group;
      let dupe = "";

      for (let i = 0; i < sack1.length; i++) {
        if (sack2.indexOf(sack1[i]) > -1 && sack3.indexOf(sack1[i]) > -1) {
          dupe = sack1[i];
          i = sack1.length;
        }
      }

      dupes.push(dupe);
      console.log(\`Dupe: \${dupe}\`);
    });

    console.log(dupes);

    let total = 0;

    dupes.forEach((dupe) => {
      if (dupe === dupe.toLowerCase()) {
        // The character is lowercase
        total += parseInt(dupe, 36) - 9;
      } else {
        // The character is uppercase
        total += parseInt(dupe, 36) + 17;
      }
    });
    return total;
  }`,
};
