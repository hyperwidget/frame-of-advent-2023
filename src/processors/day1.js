export const day1 = {
  part1: `(input) => {
    let elfCounts = [];
    let currentAcc = 0;

    input
      .replace(/\\r?\\n/g, "\\n")
      .split("\\n")
      .forEach((cal) => {
        const trimmed = cal.trim();
        if (trimmed === "") {
          elfCounts.push(currentAcc);
          currentAcc = 0;
        } else {
          currentAcc += parseInt(trimmed);
        }
      });

    elfCounts.push(currentAcc);

    return Math.max(...elfCounts);
  }`,
  part2: `(input) => {
    let elfCounts = [];
    let sorted = [];
    let currentAcc = 0;

    input
      .replace(/\\r?\\n/g, "\\n")
      .split("\\n")
      .map((cal) => cal.trim())
      .forEach((cal) => {
        if (cal === "") {
          elfCounts.push(currentAcc);
          currentAcc = 0;
        } else {
          currentAcc += parseInt(cal);
        }
      });

    elfCounts.push(currentAcc);

    sorted = elfCounts.sort((a, b) => b - a);

    return sorted[0] + sorted[1] + sorted[2];
  }`,
};
