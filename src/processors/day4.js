export const day4 = {
  part1: `(input) => {
    const pairs = input
      .trim()
      .split(/\\r?\\n/)
      .map((val) => val.trim());

    let count = 0;
    for (let i = 0; i < pairs.length; i++) {
      let assignments = pairs[i].split(",");

      const elf1 = assignments[0].split("-");
      const elf2 = assignments[1].split("-");

      elf1[0] = parseInt(elf1[0]);
      elf1[1] = parseInt(elf1[1]);
      elf2[0] = parseInt(elf2[0]);
      elf2[1] = parseInt(elf2[1]);

      if (elf1[0] <= elf2[0] && elf1[1] >= elf2[1]) {
        count++;
      } else if (elf2[0] <= elf1[0] && elf2[1] >= elf1[1]) {
        count++;
      }
    }

    return count;
  }`,
  part2: `(input) => {
    const pairs = input
      .trim()
      .split(/\\r?\\n/)
      .map((val) => val.trim());

    let count = 0;
    for (let i = 0; i < pairs.length; i++) {
      let assignments = pairs[i].split(",");

      const elf1 = assignments[0].split("-");
      const elf2 = assignments[1].split("-");

      elf1[0] = parseInt(elf1[0]);
      elf1[1] = parseInt(elf1[1]);
      elf2[0] = parseInt(elf2[0]);
      elf2[1] = parseInt(elf2[1]);

      if (elf1[0] <= elf2[0] && elf1[1] >= elf2[0]) {
        count++;
      } else if (elf2[0] <= elf1[0] && elf2[1] >= elf1[0]) {
        count++;
      }
    }

    return count;
  }`,
};
