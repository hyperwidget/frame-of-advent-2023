export const day5 = {
  part1: `(input) => {
    const rows = input.split(/\\r?\\n/);
    const stacks = [];
    const stackWords = [];
    const instructions = [];
    let instructionIndex = 0;

    for (let index = 0; index < rows.length; index++) {
      const row = rows[index];
      let cleanedRow = row.replace(/    /g, "[*]");
      cleanedRow = cleanedRow.replace(/ /g, "");
      if (cleanedRow[0] === "1") {
        instructionIndex = index;
        index = rows.length;
      } else {
        stacks.push(cleanedRow.replace(/\\[/g, "").replace(/\\]/g, ""));
      }
    }

    for (let index = 0; index < stacks[0].length; index++) {
      let word = "";

      for (let n = 0; n < stacks.length; n++) {
        word += stacks[n][index];
      }
      stackWords.push(word.split("").reverse().join("").replace(/\\*/g, ""));
    }

    for (let index = instructionIndex + 2; index < rows.length; index++) {
      let row = rows[index];
      instructions.push(
        row.replace("move ", "").replace(" from ", ",").replace(" to ", ",")
      );
    }

    instructions.forEach((instruction) => {
      const [amount, source, destination] = instruction.split(",");

      let sourceStack = stackWords[source - 1];
      let destinationStack = stackWords[destination - 1];

      for (let index = 0; index < amount; index++) {
        destinationStack += sourceStack.charAt(sourceStack.length - 1);
        sourceStack = sourceStack.slice(0, sourceStack.length - 1);
        stackWords[source - 1] = sourceStack;
        stackWords[destination - 1] = destinationStack;
      }
    });

    const resultWord = stackWords.map((stack) => stack[stack.length - 1]);

    return resultWord.join("");
  }`,
  part2: `(input) => {
    const rows = input.split(/\\r?\\n/);
    const stacks = [];
    const stackWords = [];
    const instructions = [];
    let instructionIndex = 0;
    for (let index = 0; index < rows.length; index++) {
      const row = rows[index];
      let cleanedRow = row.replace(/    /g, "[*]");
      cleanedRow = cleanedRow.replace(/ /g, "");
      if (cleanedRow[0] === "1") {
        instructionIndex = index;
        index = rows.length;
      } else {
        stacks.push(cleanedRow.replace(/\\[/g, "").replace(/\\]/g, ""));
      }
    }
    for (let index = 0; index < stacks[0].length; index++) {
      let word = "";
      for (let n = 0; n < stacks.length; n++) {
        word += stacks[n][index];
      }
      stackWords.push(word.split("").reverse().join("").replace(/\\*/g, ""));
    }
    for (let index = instructionIndex + 2; index < rows.length; index++) {
      let row = rows[index];
      instructions.push(
        row.replace("move ", "").replace(" from ", ",").replace(" to ", ",")
      );
    }
    instructions.forEach((instruction) => {
      const [amount, source, destination] = instruction.split(",");
      let sourceStack = stackWords[source - 1];
      let destinationStack = stackWords[destination - 1];

      destinationStack += sourceStack.slice(-amount);
      sourceStack = sourceStack.slice(0, -amount);
      stackWords[source - 1] = sourceStack;
      stackWords[destination - 1] = destinationStack;
    });
    const resultWord = stackWords.map((stack) => stack[stack.length - 1]);
    return resultWord.join("");
  }`,
};
