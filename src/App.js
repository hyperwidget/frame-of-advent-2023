import "./App.css";
import { Updates, TopNav } from "./components";
import { Outlet, useLocation } from "react-router-dom";
import styled from "@emotion/styled";

const Wrapper = styled("div")`
  display: flex;
  gap: 20px;
  overflow: hidden;
`;

const HiWrapper = styled("div")``;

function App() {
  let location = useLocation();

  const isRoot = location.pathname === "/";

  return (
    <div className="App">
      <TopNav />
      <div className="content">
        {isRoot && (
          <>
            <h1>Frame of Advent 2023</h1>
            <Wrapper>
              <HiWrapper>
                <p>
                  Hi! I started this year's advent of code late and without any
                  thoughts on how I was going to tackle it, so I started with an
                  online editor using JS. After completing the first two days, I
                  wanted a better way to iterate answers and test cases, share
                  solutions and maybe some sort of visualizations.
                </p>
                <p>
                  So I guess I'm making a framework? Also without any plans or
                  ideas?
                </p>
                <p>
                  I think a goal is to try to work within this framework to
                  solve the advent of code answers while also building out an
                  increasingly more and more unnecessarily complex, but terrible
                  series of tools and systems, while also building out a rough
                  site to put it on the internet. This will not be pretty; fun,
                  but not pretty.
                </p>
              </HiWrapper>
              <Updates />
            </Wrapper>
          </>
        )}
        <Outlet />
      </div>
    </div>
  );
}

export default App;
