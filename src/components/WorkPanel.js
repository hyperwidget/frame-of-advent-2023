import { Tabs } from "antd";
import { Editor } from "./";
import styled from "@emotion/styled";
import ReactMarkdown from "react-markdown";

const Wrapper = styled("div")`
  flex: 50%;
  display: flex;
  flex-direction: column;

  & .ant-tabs-tab {
    background: grey !important;
    color: white;
    &.ant-tabs-tab-active {
      background: white !important;
    }
  }

  & .ant-tabs-nav {
    margin-bottom: 0;
  }

  & .ant-tabs-top {
    height: 100%;
  }
  .ant-tabs-content-holder {
    overflow-y: scroll;
  }
`;

const MarkDown = styled(ReactMarkdown)`
  background: white;
  padding: 4px;
  text-align: left;
  max-height: 500px;
  overflow: scroll;
`;

export const WorkPanel = ({
  processor,
  reflection,
  question,
  ...editorProps
}) => {
  return (
    <Wrapper key={processor}>
      <Tabs defaultActiveKey="1" type="card" tabBarGutter={0}>
        <Tabs.TabPane tab="Editor" key="1">
          <Editor processor={processor} {...editorProps} />
        </Tabs.TabPane>
        <Tabs.TabPane tab="Question" key="2">
          <MarkDown>{question}</MarkDown>
        </Tabs.TabPane>
        <Tabs.TabPane tab="Reflection" key="3">
          <MarkDown>{reflection}</MarkDown>
        </Tabs.TabPane>
      </Tabs>
    </Wrapper>
  );
};
