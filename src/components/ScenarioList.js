import { Collapse } from "antd";
import { Input, Logs } from "./";
import {
  CheckCircleTwoTone,
  CloseCircleTwoTone,
  ClockCircleTwoTone,
} from "@ant-design/icons";

const { Panel } = Collapse;

export const ScenarioList = ({ scenarios, onInputChange, onExpectChange }) => {
  const header = (index) => {
    let icon;

    if (!scenarios[index].output) {
      icon = <ClockCircleTwoTone />;
    } else if (scenarios[index].output === scenarios[index].expect) {
      icon = <CheckCircleTwoTone twoToneColor="#52c41a" />;
    } else {
      console.log(
        `Scenario: ${index} expects: ${
          scenarios[index].expects
        }, but receieved: ${scenarios[index.output]}`
      );

      icon = <CloseCircleTwoTone twoToneColor="#eb2f96" />;
    }

    return (
      <h2>
        <span style={{ marginRight: "10px" }}>Scenario: {index + 1}</span>
        {icon}
      </h2>
    );
  };

  return scenarios.map((scenario, index) => (
    <Collapse key={index}>
      <Panel header={header(index)} key={index}>
        <Input
          value={scenario.input}
          onChange={(val) => onInputChange(index, val)}
        />
        <p>
          Expects:{" "}
          <input
            value={scenario.expect}
            onChange={({ target: { value } }) => onExpectChange(index, value)}
          />
        </p>
        {scenario.output && <p>Received: {scenario.output}</p>}
        {scenario.logs && <Logs logs={scenario.logs} />}
      </Panel>
    </Collapse>
  ));
};
