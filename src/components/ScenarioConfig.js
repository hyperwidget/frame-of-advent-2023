import { ScenarioList } from "./ScenarioList";
import { Button } from "antd";
import { useParams } from "react-router-dom";
import styled from "@emotion/styled";

const Wrapper = styled("div")`
  flex: 50%;
  overflow-y: scroll;
  display: flex;
  flex-direction: column;
`;

const DayLink = styled("a")`
  &:visited,
  &:hover,
  &:active {
    color: inherit;
  }
`;

export const ScenarioConfig = ({
  scenarios,
  handleInputChange,
  handleExpectChange,
  setScenarios,
  name,
}) => {
  const { day, part } = useParams();

  return (
    <Wrapper>
      <h3 style={{ float: "left" }}>
        <DayLink
          href={`https://adventofcode.com/2022/day/${day}`}
          rel="noreferrer"
          target="_blank"
        >
          Day {day}: Part {part} - {name}
        </DayLink>
      </h3>

      <ScenarioList
        scenarios={scenarios}
        onInputChange={handleInputChange}
        onExpectChange={handleExpectChange}
      />
      <Button
        onClick={() =>
          setScenarios([].concat(scenarios, { input: "", output: "" }))
        }
      >
        Add Scenario
      </Button>
    </Wrapper>
  );
};
