import AceEditor from "react-ace";
import styled from "@emotion/styled";
import { Button } from "antd";
import { useEffect } from "react";

import "ace-builds/src-noconflict/mode-java";
import "ace-builds/src-noconflict/theme-github";
import "ace-builds/src-noconflict/ext-language_tools";

const ButtonWrapper = styled("div")`
  display: flex;
`;

const ProcessButton = styled(Button)`
  flex: 1;
`;

export const Editor = ({
  setEditProcessor,
  processor,
  handleProcess,
  handleProcessInput,
  handleReset,
}) => {
  useEffect(() => {
    const callback = (e) => {
      if ((e.metaKey || e.ctrlKey) && e.code === "Enter") {
        e.preventDefault();
        handleProcess();
      }
    };

    document.addEventListener("keydown", callback);

    return () => {
      document.removeEventListener("keydown", callback);
    };
  });

  return (
    <>
      <AceEditor
        mode="javascript"
        theme="github"
        onChange={setEditProcessor}
        name="UNIQUE_ID_OF_DIV"
        editorProps={{ $blockScrolling: true }}
        width="100%"
        defaultValue={processor.toString()}
        style={{ overflowY: "scroll" }}
        setOptions={{
          enableBasicAutocompletion: true,
          enableLiveAutocompletion: true,
          enableSnippets: false,
          showLineNumbers: true,
          tabSize: 2,
          useWorker: false,
        }}
      />
      <ButtonWrapper>
        <ProcessButton onClick={handleProcess}>Process</ProcessButton>
        <Button onClick={handleReset}>Reset</Button>
      </ButtonWrapper>
      <ButtonWrapper>
        <ProcessButton onClick={handleProcessInput}>
          Process Input
        </ProcessButton>
      </ButtonWrapper>
    </>
  );
};
