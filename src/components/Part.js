import { useState, useEffect } from "react";
import { PartWrapper, ScenarioConfig, WorkPanel } from "./";
import { notification } from "antd";
import { useParams } from "react-router-dom";

export const Part = ({
  seedScenarios,
  processor,
  input,
  question,
  reflection,
}) => {
  const [scenarios, setScenarios] = useState(seedScenarios.tests);
  const [editProcessor, setEditProcessor] = useState(processor.toString());
  const [initialProcessor, setInitialProcessor] = useState();
  const [api, contextHolder] = notification.useNotification();
  const { day, part } = useParams();

  useEffect(() => {
    setScenarios(seedScenarios.tests);
  }, [seedScenarios]);

  useEffect(() => {
    setEditProcessor(processor.toString());
  }, [processor]);

  useEffect(() => {
    const storageProcessor = localStorage.getItem(`processor:${day}:${part}`);
    if (storageProcessor && storageProcessor !== processor) {
      setInitialProcessor(storageProcessor);
      setEditProcessor(storageProcessor);
    } else {
      setInitialProcessor(processor.toString());
    }
  }, []);

  useEffect(() => {
    setInitialProcessor(processor.toString());
  }, [day, part]);

  useEffect(() => {
    var orgLog = console.log;

    console.log = function (message) {
      if (message.includes("scen:")) {
        alert("Intercepted -> " + message); //Call Remote API to log the object.
      } else {
        //Invoke the original console.log
        return orgLog(message);
      }
    };

    return () => (console.log = orgLog);
  });

  const openNotification = ({ message, description }) => {
    api.open({
      message,
      description,
      duration: 0,
    });
  };

  const handleProcess = () => {
    try {
      scenarios.forEach((scenario, index) => {
        var orgLog = console.log;
        const logs = [];
        console.log = function (message) {
          if (!message.includes("scen")) {
            console.log(`scen:${index}--${message}`);
          } else {
            if (message.includes(`scen:${index}`)) {
              logs.push(message.replace(`scen:${index}--`, ""));
            }
          }
        };

        const runner = eval(editProcessor);
        const value = runner(scenario.input);
        const newArr = scenarios.slice();
        newArr[index].output = value;
        newArr[index].logs = logs;
        setScenarios(newArr);
        console.log = orgLog;
      });
    } catch (e) {
      openNotification({
        message: "Error!",
        description: e.message,
      });
      console.error(e);
    }
  };

  const handleInputChange = (index, val) => {
    const newArr = scenarios.slice();
    newArr[index].input = val;
    setScenarios(newArr);
  };

  const handleExpectChange = (index, val) => {
    const newArr = scenarios.slice();
    newArr[index].expect = parseInt(val);
    setScenarios(newArr);
  };

  const handleProcessInput = () => {
    const runner = eval(editProcessor);
    try {
      const value = runner(input);
      openNotification({
        message: "Run Completed",
        description: `Received Value: ${value}`,
      });
    } catch (e) {
      openNotification({
        message: "Error!",
        description: e.message,
      });
      console.error(e);
    }
  };

  const handleEditorChange = (val) => {
    localStorage.setItem(`processor:${day}:${part}`, val);
    setEditProcessor(val);
  };

  const handleReset = () => {
    setInitialProcessor();
    setInitialProcessor(processor.toString());
    setEditProcessor(processor.toString());
    localStorage.setItem(`processor:${day}:${part}`, processor.toString());
  };

  return (
    <PartWrapper>
      {contextHolder}

      <ScenarioConfig
        handleExpectChange={handleExpectChange}
        handleInputChange={handleInputChange}
        scenarios={scenarios}
        setScenarios={setScenarios}
        name={seedScenarios.name}
      />

      {initialProcessor && (
        <WorkPanel
          setEditProcessor={handleEditorChange}
          processor={initialProcessor}
          handleProcess={handleProcess}
          handleProcessInput={handleProcessInput}
          handleReset={handleReset}
          question={question}
          reflection={reflection}
        />
      )}
    </PartWrapper>
  );
};
