import styled from "@emotion/styled";

export const PartWrapper = styled("div")`
  width: 100%;
  display: flex;
  overflow: hidden;
  gap: 8px;
`;
