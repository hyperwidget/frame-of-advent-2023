import { Part } from "./";
import * as processors from "../processors";
import * as scenarios from "../scenarios";
import * as inputs from "../inputs";
import * as reflections from "../reflections";
import * as questions from "../questions";
import { useParams } from "react-router-dom";

export const Day = () => {
  const { day, part } = useParams();

  const processor = processors[`day${day}`][`part${part}`];
  const reflection = reflections[`day${day}`][`part${part}`];
  const question = questions[`day${day}`][`part${part}`];
  const seedScenarios = scenarios[`day${day}`][`part${part}`];
  const input = inputs[`day${day}`];

  return (
    <>
      <Part
        seedScenarios={seedScenarios}
        processor={processor}
        input={input}
        reflection={reflection}
        question={question}
      />
    </>
  );
};
