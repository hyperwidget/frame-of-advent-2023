import styled from "@emotion/styled";

const StyledLegend = styled("legend")`
  text-align: left;
`;
const LogWrapper = styled("fieldset")`
  display: flex;
  flex-direction: column;
`;
const LogMessage = styled("span")`
  text-align: left;
`;

export const Logs = ({ logs }) => {
  return (
    <LogWrapper>
      <StyledLegend>Logs</StyledLegend>
      {logs.map((log, i) => (
        <LogMessage key={log + i}>{log}</LogMessage>
      ))}
    </LogWrapper>
  );
};
