import { Menu } from "antd";
import styled from "@emotion/styled";
import { Link } from "react-router-dom";

const { SubMenu } = Menu;

const GitlabIcon = styled("img")`
  max-height: 100%;
  max-width: 100%;
`;
const Wrapper = styled("div")`
  display: flex;
  max-height: 46px;
`;
const GitlabLink = styled("a")`
  max-width: 60px;
  margin: auto;
`;
const dayCount = 7;
const isDone = true;
const is1Done = true;
const is2Done = true;

const renderDays = (count) =>
  [...Array(count)].map((_, index) => {
    const i = index + 1;
    return (
      <SubMenu
        title={`Day ${i}`}
        key={i}
        style={(i === dayCount && !isDone && { background: "#ffa940" }) || {}}
      >
        <Menu.Item
          key={`day${i}:part:1`}
          style={
            (i === dayCount && !is1Done && { background: "#ffa940" }) || {}
          }
        >
          <Link to={`day/${i}/part/1`}>Part 1</Link>
        </Menu.Item>
        <Menu.Item
          key={`day${i}:part:2`}
          style={
            (i === dayCount && !is2Done && { background: "#ffa940" }) || {}
          }
        >
          <Link to={`day/${i}/part/2`}>Part 2</Link>
        </Menu.Item>
      </SubMenu>
    );
  });

export const TopNav = () => {
  return (
    <Wrapper>
      <Menu mode="horizontal">
        <Menu.Item key="home">
          <Link to="/">❄️</Link>
        </Menu.Item>
        {renderDays(dayCount)}
      </Menu>
      <GitlabLink
        href="https://gitlab.com/hyperwidget/frame-of-advent-2023"
        alt="The gitlab project for this site"
        title="Gitlab!"
        target="_blank"
      >
        <GitlabIcon src="https://uxwing.com/wp-content/themes/uxwing/download/brands-and-social-media/gitlab-icon.png" />
      </GitlabLink>
    </Wrapper>
  );
};
