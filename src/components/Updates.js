import styled from "@emotion/styled";

const Panel = styled("div")`
  border-radius: 15px;
  background-color: #f0f0f0;
  color: black;
  min-width: 300px;
  overflow: scroll;

  & ul {
    list-style: none;
    padding: 0;
    & li {
      list-style: none;
    }
  }
`;

export const Updates = () => {
  return (
    <Panel>
      <h4>Latest Updates</h4>

      <ul>
        <li>
          <strong>Day 7</strong>
          <li>Completed Daily Challenge</li>
          <strong>Dec 6</strong>
          <li>Scaffold Day 7</li>
          <li>Update dayGenerator</li>
          <li>Add reflections for each question</li>
          <li>Add question definition data</li>
          <li>Add questions and reflection panels to editor</li>
          <li>Completed Daily Challenge</li>
          <strong>Dec 5</strong>
          <ul>
            <li>Started ReadMe</li>
            <li>
              Added gitlab button (can you believe they don't have those?)
            </li>
            <li>Added additonal scaffolding script tooling</li>
            <li>Added Log output for each scenario</li>
            <li>Added keylistener to editor to process</li>
            <li>Added caching of editor and ability to reset</li>
            <li>Completed daily question</li>
            <li>Added this updates panel</li>
          </ul>
        </li>
      </ul>
    </Panel>
  );
};
