import styled from "@emotion/styled";

const ScenarioInput = styled(`textarea`)`
  width: 100%;
  height: 100%;
`;

export const Input = ({ onChange, ...props }) => {
  return (
    <ScenarioInput
      {...props}
      onChange={({ target: { value } }) => onChange(value)}
    />
  );
};
