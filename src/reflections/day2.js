export const day2 = {
  part1: `# D2P1

  This was relatively straight forward, but my first pass at it totally missed the fact that X (my rock) beats C (their scissors). Missing this detail actually tripped me up in part one for a while - that was my main hiccup, and I think that the solution I ended up with shows the grossness
  
  
  # My Solution In Psuedo Code
  
  * Set up point mappers for each possible value
  * Set up a tracker variables
    * myScore for my total points over all rounds
  * Break the input into iterable rounds
  * Iterate through each round
    * Split the round into each player
    * Determine the amount of points that each player gets for their selection
    * Then we need to determine the winner of the round, I do this with an if/else
      * If their score is higher than mine - that means that their hand beats mine in every situation except
        * If they play C (scissors) and I play X (rock), I should win instead
      * Similarly if my score is higher than theirs that means my hand beats theirs
        * EXCEPT! In the wonky situation mentioned above
      * And finally, if we have the same score, that means we played the same hand, so we tie
    * At the end of the round, add this round's score to myScore
  * myScore is the final value`,
  part2: `# D2P2

  After the hiccups I was hitting on Part 1 I was a little worried about repurposing the first solution - as it turns out, I didn't reuse much from the first part. I basically just used a switch case approach for each possible option that the opponent could choose and basically brute forced my way through. Extremely nothing clever here.
  
  # My Solution In Psuedo Code
  
  * Set up point mappers for each possible value
  * Set up a tracker variables
    * myScore -- for my total points over all rounds
  * Break the input into iterable rounds
  * Iterate through each round
    * Split the round into each player
    * Determine the amount of points that the opponent will get for their selection
    * myVal -- set up a tally var for my score this round
    * Then we need to determine whether I should win/lose/tie, and what value I need to play in order to achieve that outcome
      * If I should Lose
        * create a case for each opponent action (a, b, c)
          * determine what value would lose to that action and add the value for that action to myVal
          * because no points are awarded for losing, no additional points are awarded
      * If I should win
        * create a case for each opponent action (a, b, c)
          * determine what value would win against that action and add the value for that action to myVal
          * Also add the winning value of 6 to myVal
      * If I should tie then add the same amount of points the opponent had
        * Also add the 3 points for tying with my opponent
    * At the end of the round, add this round's myVal score to myScore
  * myScore is the final value`,
};
