export const day7 = {
  part1: `# D7P1
I kinda felt like today's puzzle would give me issues, and I wasn't totally wrong. I originally started down a path that ended up very similar to my final solution; however there was a "gotcha" that I wasn't sure would pop up; my original solution wasn't going to work with directory structures with more than one child folder. As a result, I ended up changing my approach juuuust a bit to include some recursion.

When I saw that this was just a tree, I considered just setting everything into one object, but then I "normalized" the data for ease of access and inspection and I think that was probably the right call; at the very least for traversal reasons. This is proooooobably going to be a bit hairy to explain in psuedocode, the current solution is pretty ugly and is probably the first candidate for me to go back and revisit.

(Side note: I don't think I'm going to get any time working on the framework today after the amount of time this solution took me:shrug:)

# My Solution In Psuedo Code

* Break the input into iterable rows
* (I'll talk about the recursor method later)
* directory -- the object that will store all the data about our file system
* currentDirectory -- a variable to track the name of the directory we're currently traversing
* eligibleDirectories -- an array to hold all the potential directories we could delete
* And while I'm talking about directories, let's look at what their structure looks like - every directory has the following attributes:
	* parent -- the name of this directory's parent
	* children -- all the child directories of this one
	* files -- the files included in this directory, along with their values
	* fileTotal -- a sum of the values of all the files in the fileList
	* additionally there is a value that gets added to the directory later in
		* encompasingTotal -- variable to hold the value of this directory's files as well as the total value of all of this directory's children
* Let's start processing each line
* For each line
	* if this line starts with \`$\` we know that it's a command, so let's act on it
		* Grab the instruction and the directory name from the command by splitting
		* We only care about the \`cd\` action, so let's handle that
			* if we're asked to traverse \`..\` we change currentDirectory to our currentDirectory's parent
			* if we're asked to traverse to any other directory
				* This is our first time in this directory, so scaffold this directory into the directory variable
				* And change currentDirectory to this directory
				* You'll see I have \`\${currentDirectory}.\${dir}\` as the names of the directory - I did this as we can have multiple directories of the same name, so this basically gives me a unique name for every folder that includes its full path!
			* This is all we need to do with the cd command and the commands in general
		* Since this line doesn't start with \`$\` we know it's either a new directory or a file
		* If it's a new directory
			* add to  currentDirectory.children array
		* Otherwise it's a file
			* add to currentDirectory.files
			* add the size of the file to currentDirectory.totalSize

So at this state, we basically have a dictionary of all folders, their children, and the size of the files that are _only_ in their directory. What we do now is use a recursion method to traverse this directory and sum up the totalSize of each directory's children so that we can add it to the totalSize (encompasingTotal). So let's look at the \`recursor\` method now.

* This method takes the full directory, as well as the current directory we're working in.
* tally -- a variable to track the totalSize of all of this directory's children recursively
* for each child in this directory
	* grab the child's directory data from the directory object
	* add this child's totalSize to the tally variable
	* pass this child node _back_ into the recursion method
		* This will return the size of all of our child folders recursively
	* add the value from the recursion call to the tally
	* tally is now the totalSize of this directory's childSize

We use this recursion method to calculate the the total size of each directory including their children, and if that size is less than 100000 we add it to eligibleDirectories

So now we have all the possible eligible directories, all that's left is to get our final answer from them which is a case of iterating through all the elibles and adding their total sizes together

Whew. Done!
`,
  part2: `# D7P2
  
  K, so part 2 is mostly a riff on the first part from my solution, except now we need to do some math when determining the \`eligibleDirectories\`. We're able to re-use almost all of the logic from the first part, except we do something a little different with the results, that'll be the part I write about here.
  
  # My Solution In Psuedo Code
  * At this point we have used this recursion method to calculate the the total size of each directory including their children
  * Now that we have specific sizes and requirements, let's quickly determine how much space we actually need to free up
  * We know how much total space is in use because we know how much size is used by "./"
  * Because of that and the fact that we know the total size of the system, we can subtract the amount of used space from total space and we now know how much space we have to work with currently
  * If we subtract that amount from the total space that we need to create, we now have the number for the amount of space that we need to clean up - this is tracked in \`neededSpace\`
  * We iterate through each directory and pick out the ones that have an encompasingTotal that is more than the needed space we have, I track this in an \`eligibleDirectories\` array again
  * I then filter the eligibleDirectories array of directories to get the size of each, that's all we care about
  * sort the sizes and return the smallest one`,
};
