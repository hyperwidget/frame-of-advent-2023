export const day4 = {
  part1: `# D4P1
  This one was also fun, but I feel like I came up with a solution that gets the right answer, but by skips a few steps that the question seems to imply it wants us to take. Basically all I did was check if the top and bottom values of either pair are outside the range of the opposite pair.
  
  # My Solution In Psuedo Code
  
  * Break the input into iterable pairs
  * count -- variable to count how many pairs are within the other pair
  * iterate through each pair
    * split each pair into their individual upper and lower bounds
      * elf1 and elf2 now each have an upper and a lower bound value
    * if elf1's lower bound is lower than elf2's and elf1's upper bound is higher than elf2's 
      * Then we know that all of elf2's values are within elf1
      * so add tally to count
    * else if elf2's lower bound is lower than elf1's and elf2's upper bound is higher than elf1's 
      * Then we know that all of elf1's values are within elf1
      * so add tally to count
  * Return count, it's the answer`,
  part2: `# D4P2
  Part 2 is super similar to part 1, however instead of counting every time there is a full overlap, we want to count any time there is _any_ overlap.
  
  # My Solution In Psuedo Code
  
  * Break the input into iterable pairs
  * count -- variable to count how many pairs are within the other pair
  * iterate through each pair
    * split each pair into their individual upper and lower bounds
      * elf1 and elf2 now each have an upper and a lower bound value
    * **This comparison part is all that changes in the answer**
    * if elf1's lower bound is lower than elf2's and elf1's upper bound is higher than elf2's _lower_ bound
      * Then we know that there is an overlap of at least 1 number between the two
      * so add tally to count
    * else if elf2's lower bound is lower than elf1's and elf2's upper bound is higher than elf1's _lower_ bound 
      * Then we know that there is an overlap of at least 1 number between the two
      * so add tally to count
  * Return count, it's the answer`,
};
