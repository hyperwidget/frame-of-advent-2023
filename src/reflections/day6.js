export const day6 = {
  part1: `# D6P1
  This might have been my shortest solution so far. I think the solution was suggesting that we iterate through each character, check if it's present plural times, and then check the previous four letters, etc.
  
  However, in order to save processing speed, I actually started with checking the group of unique four letters, and checking for the duplicated character later in the string. This is also the first solution that gave us plural test scenarios, so I finally got to make use of my various scenario runner!
  
  # My Solution In Psuedo Code
  
  * index -- variable to track the answer
  * we're going to loop through every character in the string, but we'll start at the 4th index because the answer has to be at least 4 characters
  * iterate each character
    * segment -- array to track the four characters in the segment before the current index
    * segmentSet -- a set that we generate using segment
    * since segments cannot have duplicate values we can tell if there were any duplicates by checking the length of segmentSet
    * if the length of segmentSet is 4 there are no duplicates and we've found our answer
      * set index to the index of the input that we're currently on
      * short circuit the iteration
  * return index, it's the answer`,
  part2: `# D6P2
  This part was actually _super_ quick for me, thanks to the way I programmed the first part. All I had to do was change all of the counts of 4 (which is the target length of the segment) to 14 (which is the new length), and I got the answer.
  
  # My Solution In Psuedo Code
  
  * index -- variable to track the answer
  * we're going to loop through every character in the string, but we'll start at the 14th index because the answer has to be at least 14 characters
  * iterate each character
    * segment -- array to track the four characters in the segment before the current index
    * segmentSet -- a set that we generate using segment
    * since segments cannot have duplicate values we can tell if there were any duplicates by checking the length of segmentSet
    * if the length of segmentSet is 14 there are no duplicates and we've found our answer
      * set index to the index of the input that we're currently on
      * short circuit the iteration
  * return index, it's the answer`,
};
