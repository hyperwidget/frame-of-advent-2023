export const day3 = {
  part1: `# D3P1

  Today's challenge was pretty fun for me and is the first question in this year's question set that I felt would make a super fun visualization - hopefully at some time I get to actually create that!
  
  # My Solution In Psuedo Code
  
  * Break the input into iterable "sacks"
  * dupes -- Array to hold all the duplicates
  * Iterate through each sack
    * Split the sack in half
    * dupe -- variable to hold the found duplicate value
    * iterate through each letter in the first sack so we can see if it's in the second sack
      * check the second sack for that letter
      * if the second sack contains the letter being iterated
        * we've found our duplicate, toss it into dupe
        * short circuit the loop to make stuff go quicker
      * Add dupe to the list of dupes
  * total -- a tally variable for the total value
  * We now have all of our duplicates, let's get the point value of each
    * if letter is lowercase, then determine the value for it and add it to total
     * if letter is upper, then determine the value for it and add it to total
  * Return total, it's the answer`,
  part2: `# D3P2

  This was a relatively straight forward riff on part 1, I was able to reuse most of the first answer, except instead of comparing the first half of the sack with the second half, I break the sacks into sets of three and check each letter in the first sack in the other two sacks
  
  # My Solution In Psuedo Code
  
  * Break the input into iterable "sacks"
  * groups -- Array to hold groups of 3 sacks
  * acc -- temporary array to group sacks into 3s to add into groups
  * iterate through sacks
    * each iteration add sack to acc
    * if iteration is a mod of 3
      * push acc to groups
      * reset acc to an empty array
  * dupes -- Array to hold all the duplicates
  * Iterate through each group
    * dupe -- variable to hold the found duplicate value
    * iterate through each letter in the first sack in the group so we can see if it's in the other sacks
      * check the other sacks for that letter
      * if the other sacks contains the letter being iterated
        * we've found our duplicate, toss it into dupe
        * short circuit the loop to make stuff go quicker
      * Add dupe to the list of dupes
  * total -- a tally variable for the total value
  * We now have all of our duplicates, let's get the point value of each
    * if letter is lowercase, then determine the value for it and add it to total
     * if letter is upper, then determine the value for it and add it to total
  * Return total, it's the answer`,
};
