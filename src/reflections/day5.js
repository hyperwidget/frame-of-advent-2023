export const day5 = {
  part1: `# D5P1
  This was one of their problems where actually parsing the input into a workable format was the majority of the struggle. Once I finally managed that part, the data manipulation was pretty straight forward
  
  Up to this point, this was also the solution that took the longest.

  # My Solution In Psuedo Code
  
  * Break the input into iterable rows
  * stacks -- array to track separate stacks
  * stackWords -- array of the stacks transferred into string format
  * instructions -- array to hold the manipulation instructions
  * instructionIndex -- variable to track which row the instructions start on
  * iterate through each row
    * replace every 4 spaces in the row with an indicator that matches the other crates to say "this is actually an empty space"
    * remove the remaining whitespace in the row
    * if the first index of this row is "1", we know that we've processed all the stacks
      * Set the instruction index so we know where to begin processing those
      * Short circuit the iteration loop
    * otherwise, add the crate's letters to stacks
      * Without the square brackets of course
  * This basically gives us an array of our stacks in cross slices, the next step is to take these cross slices and turn it so that we then have a horizontal representation of each stack (I want each stack to be represented as a string)
  * From here, we want to parse the instructions
  * Iterate through the remaining rows from the instruction index
    * remove all the words from the row and split the row by each number for instructions
    * iterate for the amount of crates we need to move
      * take the last letter from the source stackword
      * add that letter to the target stackword
  * iterate through the stack words
    * grab the last letter off each stackword and add it to an array
  * Return the above array after joining it into a string`,
  part2: `# D5P2
  Thankfully part 2 was able to reuse **all** of the parsing code from part 1, so the only change necessary in this part was just a minor adjustment in processing the instructions. 
  
  # My Solution In Psuedo Code
  
  * Break the input into iterable rows
  * stacks -- array to track separate stacks
  * stackWords -- array of the stacks transferred into string format
  * instructions -- array to hold the manipulation instructions
  * instructionIndex -- variable to track which row the instructions start on
  * iterate through each row
    * replace every 4 spaces in the row with an indicator that matches the other crates to say "this is actually an empty space"
    * remove the remaining whitespace in the row
    * if the first index of this row is "1", we know that we've processed all the stacks
      * Set the instruction index so we know where to begin processing those
      * Short circuit the iteration loop
    * otherwise, add the crate's letters to stacks
      * Without the square brackets of course
  * This basically gives us an array of our stacks in cross slices, the next step is to take these cross slices and turn it so that we then have a horizontal representation of each stack (I want each stack to be represented as a string)
  * From here, we want to parse the instructions
  * Iterate through the remaining rows from the instruction index
    * remove all the words from the row and split the row by each number for instructions
    * this gives us the {amount of crates to move}{target stack }{destination stack}
    * **This is the part that changed between the parts**
    * take the last {amount} of letters from the source stackword
    * add those letters to the target stackword
  * iterate through the stack words
    * grab the last letter off each stackword and add it to an array
  * Return the above array after joining it into a string`,
};
