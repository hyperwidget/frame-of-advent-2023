export const day1 = {
  part1: `# D1P1

  Another year, another advent of code! Let's go
  
  
  # My Solution In Psuedo Code
  
  * Set up tracking variables
    * elfCounts -- An array of counts, one for each elf's calorie count
    * currentAcc -- A variable to store the count of calories for each elf
  * Break the input into iterable rows
  * Iterate through each row
    * If this row is a number, add the value to currentAcc
    * Else, the current elf is done counting
      * Add the elf's value to elfCounts
  * Use Math.max on elfCounts to get the highest value`,
  part2: `# D1P2

  Pretty straightforward to update from my previous solution - since I already have _all_ of the elf counts, I can take the previous answer and just change the return value
  
  
  # My Solution In Psuedo Code
  
  * Set up tracking variables
    * elfCounts -- An array of counts, one for each elf's calorie count
    * currentAcc -- A variable to store the count of calories for each elf
  * Break the input into iterable rows
  * Iterate through each row
    * If this row is a number, add the value to currentAcc
    * Else, the current elf is done counting
      * Add the elf's value to elfCounts
  * **below is all that's changed** 
  * Sort the elf counts and return the top 3 values`,
};
