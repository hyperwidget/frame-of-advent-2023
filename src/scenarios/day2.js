export const day2 = {
  part1: {
    name: "Rock Paper Scissors",
    tests: [
      {
        input: `A Y
          B X
          C Z`,
        expect: 15,
      },
    ],
  },
  part2: {
    name: "Rock Paper Scissors",
    tests: [
      {
        input: `A Y
        B X
        C Z`,
        expect: 12,
      },
    ],
  },
};
