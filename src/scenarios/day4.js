export const day4 = {
  part1: {
    name: "Camp Cleanup",
    tests: [
      {
        input: `2-4,6-8
        2-3,4-5
        5-7,7-9
        2-8,3-7
        6-6,4-6
        2-6,4-8`,
        expect: 2,
      },
    ],
  },
  part2: {
    name: "Camp Cleanup",

    tests: [
      {
        input: `2-4,6-8
        2-3,4-5
        5-7,7-9
        2-8,3-7
        6-6,4-6
        2-6,4-8`,
        expect: 4,
      },
    ],
  },
};
