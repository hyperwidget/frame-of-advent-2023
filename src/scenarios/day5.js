export const day5 = {
  part1: {
    name: "Supply Stacks",
    tests: [
      {
        input: `    [D]    
[N] [C]    
[Z] [M] [P]
  1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2`,
        expect: `CMZ`,
      },
    ],
  },
  part2: {
    name: "Supply Stacks",

    tests: [
      {
        input: `    [D]    
[N] [C]    
[Z] [M] [P]
  1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2`,
        expect: `MCD`,
      },
    ],
  },
};
