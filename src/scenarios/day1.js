export const day1 = {
  part1: {
    name: "Calorie Counting",
    tests: [
      {
        input: `1000
          2000
          3000
          
          4000
          
          5000
          6000
          
          7000
          8000
          9000
          
          10000`,
        expect: 24000,
      },
    ],
  },
  part2: {
    name: "Calorie Counting",
    tests: [
      {
        input: `1000
          2000
          3000
          
          4000
          
          5000
          6000
          
          7000
          8000
          9000
          
          10000`,
        expect: 45000,
      },
    ],
  },
};
