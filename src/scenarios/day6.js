export const day6 = {
  part1: {
    name: "Tuning Trouble",
    tests: [
      {
        input: `mjqjpqmgbljsphdztnvjfqwrcgsmlb`,
        expect: 7,
      },
      {
        input: `bvwbjplbgvbhsrlpgdmjqwftvncz`,
        expect: 5,
      },
      {
        input: `nppdvjthqldpwncqszvftbrmjlhg`,
        expect: 6,
      },
      {
        input: `nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg`,
        expect: 10,
      },
      {
        input: `zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw`,
        expect: 11,
      },
    ],
  },
  part2: {
    name: "Tuning Trouble",
    tests: [
      {
        input: `mjqjpqmgbljsphdztnvjfqwrcgsmlb`,
        expect: 19,
      },
      {
        input: `bvwbjplbgvbhsrlpgdmjqwftvncz`,
        expect: 23,
      },
      {
        input: `nppdvjthqldpwncqszvftbrmjlhg`,
        expect: 23,
      },
      {
        input: `nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg`,
        expect: 29,
      },
      {
        input: `zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw`,
        expect: 26,
      },
    ],
  },
};
