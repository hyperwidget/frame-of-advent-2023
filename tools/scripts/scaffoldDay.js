const replace = require("replace-in-file");

const fs = require("fs-extra");

const copy = (location, target) =>
  fs
    .copy(location, target)
    .then(() => {})
    .catch((err) => console.error(err));
module.exports.copy = copy;

module.exports.generateNewDay = async (dayNumber) => {
  const name = `day${dayNumber}`;

  const templatePath = `${__dirname}/../templates`;
  const baseTargetPath = `${__dirname}/../../src`;

  const scenariosTemplatePath = `${templatePath}/scenarios.js`;
  const inputTemplatePath = `${templatePath}/input.js`;
  const processorsTemplatePath = `${templatePath}/processors.js`;
  const questionsTemplatePath = `${templatePath}/questions.js`;
  const reflectionsTemplatePath = `${templatePath}/reflections.js`;

  const scenariosTargetPath = `${baseTargetPath}/scenarios/`;
  const inputTargetPath = `${baseTargetPath}/inputs/`;
  const processorsTargetPath = `${baseTargetPath}/processors/`;
  const questionsTargetPath = `${baseTargetPath}/questions/`;
  const reflectionsTargetPath = `${baseTargetPath}/reflections/`;

  const scenariosFinalPath = `${scenariosTargetPath}/${name}.js`;
  const inputFinalPath = `${inputTargetPath}/${name}.js`;
  const processorsFinalPath = `${processorsTargetPath}/${name}.js`;
  const questionsFinalPath = `${questionsTargetPath}/${name}.js`;
  const reflectionsFinalPath = `${reflectionsTargetPath}/${name}.js`;

  const options = {
    files: [
      scenariosFinalPath,
      inputFinalPath,
      processorsFinalPath,
      questionsFinalPath,
      reflectionsFinalPath,
    ],
    from: [/DAY_NAME/g],
    to: [name],
  };

  await copy(scenariosTemplatePath, scenariosFinalPath);
  await copy(inputTemplatePath, inputFinalPath);
  await copy(processorsTemplatePath, processorsFinalPath);
  await copy(questionsTemplatePath, questionsFinalPath);
  await copy(reflectionsTemplatePath, reflectionsFinalPath);

  /* Replace template string variables in newly created folder */
  replace.sync(options);

  // Add new file to barrel for auto imports
  fs.appendFileSync(
    `${scenariosTargetPath}/index.js`,
    `export * from "./${name}.js";`
  );
  fs.appendFileSync(
    `${inputTargetPath}/index.js`,
    `export * from "./${name}.js";`
  );
  fs.appendFileSync(
    `${processorsTargetPath}/index.js`,
    `export * from "./${name}.js";`
  );
  fs.appendFileSync(
    `${questionsTargetPath}/index.js`,
    `export * from "./${name}.js";`
  );
  fs.appendFileSync(
    `${reflectionsTargetPath}/index.js`,
    `export * from "./${name}.js";`
  );

  console.log(`${name} created ✨`);
};
